#!/usr/bin/zsh

solar=$(solaar show 2> /dev/null)
battery=$(echo $solar | grep 'Battery' | head -n 1)

if [ -z "$battery" ]
then
   echo ""
else
  percentage=$(echo $battery | cut -d ":" -f 2 | cut -d "%" -f 1 | xargs)
  charging=""
  if [[ $battery == *"recharging"* ]]
  then
    charging=" (charging)"
  fi
  echo $percentage"%"$charging
fi

