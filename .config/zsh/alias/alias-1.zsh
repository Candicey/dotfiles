alias "lunar"="screen ~/Applications/Game/LunarClient/run1.sh"
alias "lunar-socket"="npm --prefix=~/PhpstormProjects/LunarSocket run start"
#alias "figma"="LD_PRELOAD=/usr/lib/libm.so.6 /usr/bin/figma-linux"
alias "py"="python3"
alias "v"="nvim"
alias "lock"="i3lock -c 000000"
alias "l"="lsd"
alias "ll"="lsd -l"
alias "van"="man --pager 'nvim -c Man\!'" # (neoVim mAN)
alias "g"="grep"
alias ":q"="exit"
alias "dfg"="/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME" # (Dotfiles ConfiG) https://youtu.be/tBoLDpTWVOM

