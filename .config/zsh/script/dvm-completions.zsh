#compdef dvm

autoload -U is-at-least

_dvm() {
    typeset -A opt_args
    typeset -a _arguments_options
    local ret=1

    if is-at-least 5.2; then
        _arguments_options=(-s -S -C)
    else
        _arguments_options=(-s -C)
    fi

    local context curcontext="$curcontext" state line
    _arguments "${_arguments_options[@]}" \
'-h[Prints help information]' \
'--help[Prints help information]' \
'-V[Prints version information]' \
'--version[Prints version information]' \
":: :_dvm_commands" \
"*::: :->dvm" \
&& ret=0
    case $state in
    (dvm)
        words=($line[1] "${words[@]}")
        (( CURRENT += 1 ))
        curcontext="${curcontext%:*:*}:dvm-command-$line[1]:"
        case $line[1] in
            (install)
_arguments "${_arguments_options[@]}" \
'-v[verbosity]' \
'--verbose[verbosity]' \
'-h[Prints help information]' \
'--help[Prints help information]' \
'-V[Prints version information]' \
'--version[Prints version information]' \
'::type:(stable discord-stable s canary discord-canary c ptb discord-ptb p development dev discord-development d)' \
&& ret=0
;;
(update)
_arguments "${_arguments_options[@]}" \
'-h[Prints help information]' \
'--help[Prints help information]' \
'-V[Prints version information]' \
'--version[Prints version information]' \
'::type:(stable discord-stable s canary discord-canary c ptb discord-ptb p development dev discord-development d)' \
&& ret=0
;;
(show)
_arguments "${_arguments_options[@]}" \
'-h[Prints help information]' \
'--help[Prints help information]' \
'-V[Prints version information]' \
'--version[Prints version information]' \
'::type:(stable discord-stable s canary discord-canary c ptb discord-ptb p development dev discord-development d)' \
&& ret=0
;;
(remove)
_arguments "${_arguments_options[@]}" \
'-h[Prints help information]' \
'--help[Prints help information]' \
'-V[Prints version information]' \
'--version[Prints version information]' \
'::type:(stable discord-stable s canary discord-canary c ptb discord-ptb p development dev discord-development d)' \
&& ret=0
;;
(completions)
_arguments "${_arguments_options[@]}" \
'-h[Prints help information]' \
'--help[Prints help information]' \
'-V[Prints version information]' \
'--version[Prints version information]' \
'::type:(bash b elvish e fish f powershell pwsh ps p zsh z)' \
&& ret=0
;;
(help)
_arguments "${_arguments_options[@]}" \
'-h[Prints help information]' \
'--help[Prints help information]' \
'-V[Prints version information]' \
'--version[Prints version information]' \
&& ret=0
;;
        esac
    ;;
esac
}

(( $+functions[_dvm_commands] )) ||
_dvm_commands() {
    local commands; commands=(
        "install:install the latest <type> of discord" \
"update:update to the latest <type> of discord" \
"show:show all installed versions" \
"remove:remove the installed <type> of discord" \
"completions:get shell completions" \
"help:Prints this message or the help of the given subcommand(s)" \
    )
    _describe -t commands 'dvm commands' commands "$@"
}
(( $+functions[_dvm__completions_commands] )) ||
_dvm__completions_commands() {
    local commands; commands=(

    )
    _describe -t commands 'dvm completions commands' commands "$@"
}
(( $+functions[_dvm__help_commands] )) ||
_dvm__help_commands() {
    local commands; commands=(

    )
    _describe -t commands 'dvm help commands' commands "$@"
}
(( $+functions[_dvm__install_commands] )) ||
_dvm__install_commands() {
    local commands; commands=(

    )
    _describe -t commands 'dvm install commands' commands "$@"
}
(( $+functions[_dvm__remove_commands] )) ||
_dvm__remove_commands() {
    local commands; commands=(

    )
    _describe -t commands 'dvm remove commands' commands "$@"
}
(( $+functions[_dvm__show_commands] )) ||
_dvm__show_commands() {
    local commands; commands=(

    )
    _describe -t commands 'dvm show commands' commands "$@"
}
(( $+functions[_dvm__update_commands] )) ||
_dvm__update_commands() {
    local commands; commands=(

    )
    _describe -t commands 'dvm update commands' commands "$@"
}

_dvm "$@"

