#!/bin/env sh

/usr/local/bin/bk /home/mon/.config/rofi/music/notification.sh $@
pid=$(ps x | awk '{split($0,a," ")}; { if (a[5]=="mpv") { if (a[6]=="--profile=music") { if (a[7]=="--no-video") { if (a[8]=="--loop") { print a[1] } } } } }')
if [ ! -z "$pid" ]
then
  kill $pid
  sleep 0.75
  mpv --profile=music --no-video --loop --volume=50 "$@"
else
  mpv --profile=music --no-video --loop --volume=50 "$@"
fi
