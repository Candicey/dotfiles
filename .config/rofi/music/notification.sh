#!/bin/env sh

title=$(youtube-dl -e $@)
/usr/bin/notify-send.py "Now Playing" "$title"
time=$(date +%T)
echo "[ $time ] $@ | $title" >> ~/Music/ytme.log

