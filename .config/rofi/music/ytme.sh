#!/bin/env sh

song=$(/usr/local/bin/history-manager --id ytme | /usr/bin/rofi -dmenu -p "Song:")

if [ -z "$song" ]
then
  exit 0
fi

/usr/local/bin/history-manager --id ytme --add "$song"
#/usr/local/bin/bk /usr/local/bin/mpm kill
#echo "{ command: ['quit'] }" | socat - /tmp/mpvsocket-music
#kill -9 $(ps x | awk '{split($0,a," ")}; { if (a[5]=="mpv") { if (a[6]=="--profile=music") { if (a[7]=="--no-video") { if (a[8]=="--loop") { print a[1] } } } } }')
#pid=$(ps x | awk '{split($0,a," ")}; { if (a[5]=="mpv") { if (a[6]=="--profile=music") { if (a[7]=="--no-video") { if (a[8]=="--loop") { print a[1] } } } } }')

#if [ -z "$pid" ]
#then
#  /usr/bin/alacritty --title "[i3-wm; floating] Song Selection" --command yt -c '/home/mon/.config/rofi/music/mpvc-kill.sh && /usr/local/bin/bk mpv --profile=music --no-video --loop --volume=50"' $song
#else
#  /usr/bin/alacritty --title "[i3-wm; floating] Song Selection" --command yt -c '/usr/local/bin/bk mpv --profile=music --no-video --loop --volume=50' $song
#fi

/usr/bin/alacritty --title "[i3-wm; floating] Song Selection" --command yt -c "/home/mon/.config/rofi/music/process-input.sh" "$song"

